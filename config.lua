system = {
  ticks_per_second = 25,
}
window = {
  title = "Tajga ~ Tetris",
  width = 800,
  height = 600,
  fullscreen = false,
}
text = {
  font = "assets/Roboto-Regular.ttf",
  size = 50,
}
small_text = {
  font = "assets/Roboto-Regular.ttf",
  size = 20,
}
tetris = {
  move_speed_first = 5.0,
  move_speed_rest = 20.0,
  soft_drop_speed = 30.0,
  rules = {
    -- The level increases after this many line clears.
    clears_per_level = 10,
    -- Enables scoring for T-Spin.
    enable_t_spin = true,
    -- Enables scoring for T-Spin Mini.
    enable_t_spin_mini = true,
    -- Changes the T-Spin recognition.
    -- T-Spin requires to have 3 out of 4 corners occupied.
    -- If this is true, corners out of the board are considered occupied.
    is_border_considered_occupied = true,
    -- Enables back-to-back T-Spin and Tetris clear scoring bonus.
    enable_back_to_back = true,
    -- Enables consecutive line clears scoring bonus.
    enable_combo = true,
    -- Amount of points per dropped row via soft drop.
    soft_drop_per_cell_score = 1,
    -- Amount of points per dropped row via hard drop.
    hard_drop_per_cell_score = 2,
  },
  board = {
    width = 10,
    height = 22,
    buffer_height = 2,

    view = {
      position = {20, 20},
      block_gap = 2,
      block_size = 24,
      display_buffer = false,
    },
  },
}
options = {
  starting_level = 1,
  display_ghost = true,
}
