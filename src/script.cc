#include "script.h"

#include <SDL2/SDL_log.h>

namespace tajga {

LuaError::LuaError(lua_State * lua_state) noexcept
  : std::runtime_error(lua_tostring(lua_state, -1))
{}

LuaVariableError::LuaVariableError(std::string const & message, std::string const & identificator) noexcept
  : std::runtime_error("'" + identificator + "': " + message)
{}

class InvalidLuaVariableNameError : public LuaVariableError {
public:
  explicit InvalidLuaVariableNameError(std::string const & identificator) noexcept
    : LuaVariableError("Invalid LUA variable name", identificator)
  {}
}; // class InvalidLuaVariableNameError

ScriptEngine::ScriptEngine()
: lua_state_(luaL_newstate()) {
  if (lua_state_ == nullptr) {
    throw std::runtime_error("lua new state failed");
  }
  luaL_openlibs(lua_state_);
}

ScriptEngine::~ScriptEngine() noexcept {
  if (lua_state_ != nullptr) {
    lua_close(lua_state_);
  }
}

void ScriptEngine::DoFile(char const * file) {
  SDL_assert(file != nullptr);
  SDL_Log("Executing lua file '%s'", file);

  if (luaL_dofile(lua_state_, file)) {
    throw LuaError(lua_state_);
  }
}

void ScriptEngine::DoString(char const * str) {
  SDL_assert(str != nullptr);
  SDL_Log("Executing lua string '%s'", str);

  if (luaL_dostring(lua_state_, str)) {
    throw LuaError(lua_state_);
  }
}

void ScriptEngine::LoadVariable(std::string const & identificator) const {
  SDL_LogVerbose(SDL_LOG_CATEGORY_APPLICATION, "Loading lua variable '%s'", identificator.c_str());
  auto const kDelimiter = ".";

  size_t p1 = identificator.find_first_of(kDelimiter);

  // If it is a simple identifier, no parsing is needed
  if (p1 == std::string::npos) {
    auto r = lua_getglobal(lua_state_, identificator.c_str());
    if (r == LUA_TNIL) {
      throw InvalidLuaVariableNameError(identificator);
    }
    return;
  }

  // Pushes the first token
  auto token = identificator.substr(0, p1);
  if (lua_getglobal(lua_state_, token.c_str()) == LUA_TNIL) {
    throw InvalidLuaVariableNameError(identificator);
  }
  auto p0 = p1 + 1;

  while ((p1 = identificator.find_first_of(kDelimiter, p0)) != std::string::npos) {
    token = identificator.substr(p0, p1 - p0);
    if (lua_getfield(lua_state_, -1, token.c_str()) == LUA_TNIL) {
      throw InvalidLuaVariableNameError(identificator);
    }
    lua_replace(lua_state_, -2);
    p0 = p1 + 1;
  }

  // Pushes the last token
  token = identificator.substr(p0);
  if (lua_getfield(lua_state_, -1, token.c_str()) == LUA_TNIL) {
    throw InvalidLuaVariableNameError(identificator);
  }
  lua_replace(lua_state_, -2);
}

} // namespace tajga
