#include "player_tetromino.h"

#include <SDL2/SDL_log.h>

#include "board.h"

namespace {
using tajga::Board;
using tajga::Tetromino;

using tajga::RotationTest;
using tajga::WallKickData;

// J, L, S, T, Z Tetromino Wall Kick Data
static const WallKickData kWallKickData_JLSTZ = {
  // right rotation from 0 or (inverted values for) left rotation from 1
  RotationTest{ SDL_Point{-1,0}, SDL_Point{-1,-1}, SDL_Point{0,2}, SDL_Point{-1,2} },
  // right rotation from 1 or (inverted values for) left rotation from 2
  RotationTest{ SDL_Point{1,0}, SDL_Point{1,1}, SDL_Point{0,-2}, SDL_Point{1,-2} },
  // right rotation from 2 or (inverted values for) left rotation from 3
  RotationTest{ SDL_Point{1,0}, SDL_Point{1,-1}, SDL_Point{0,2}, SDL_Point{1,2} },
  // right rotation from 3 or (inverted values for) left rotation from 4
  RotationTest{ SDL_Point{-1,0}, SDL_Point{-1,1}, SDL_Point{0,-2}, SDL_Point{-1,-2} },
};

// I Tetromino Wall Kick Data
static const WallKickData kWallKickData_I = {
  // right rotation from 0 or (inverted values for) left rotation from 1
  RotationTest{ SDL_Point{-2,0}, SDL_Point{1,0}, SDL_Point{-2,1}, SDL_Point{1,-2} },
  // right rotation from 1 or (inverted values for) left rotation from 2
  RotationTest{ SDL_Point{-1,0}, SDL_Point{2,0}, SDL_Point{-1,-2}, SDL_Point{2,1} },
  // right rotation from 2 or (inverted values for) left rotation from 3
  RotationTest{ SDL_Point{2,0}, SDL_Point{-1,0}, SDL_Point{2,-1}, SDL_Point{-1,2} },
  // right rotation from 3 or (inverted values for) left rotation from 4
  RotationTest{ SDL_Point{1,0}, SDL_Point{-2,0}, SDL_Point{1,2}, SDL_Point{-2,-1} },
};

}  // namespace

namespace tajga {

PlayerTetromino::PlayerTetromino(Tetromino tetromino, SDL_Point position) noexcept
    : tetromino_(tetromino),
      position_(position) {
}

bool CheckCollision(Board const & board,
    Tetromino::Shape const & shape, SDL_Point const & p) noexcept {
  SDL_assert(p.x + Tetromino::kSize > 0);
  SDL_assert(p.x < board.width());
  SDL_assert(p.y < board.height());

  // Checks whether any block of the map is out of bounds to the left
  if (p.x < 0) {
    for (auto const & block : shape) {
      if (p.x + block.x < 0) {
        return false;
      }
    }
  }
  // Checks whether any block of the map is out of bounds to the right
  if (p.x + Tetromino::kSize > board.width()) {
    for (auto const & block : shape) {
      if (p.x + block.x >= board.width()) {
        return false;
      }
    }
  }
  // Checks whether any block of the map is out of bounds at the bottom
  if (p.y + Tetromino::kSize > board.height()) {
    for (auto const & block : shape) {
      if (p.y + block.y >= board.height()) {
        return false;
      }
    }
  }
  // Checks whether any block of the map is out of bounds at the top
  if (p.y < 0) {
    for (auto const & block : shape) {
      if (p.y + block.y < 0) {
        return false;
      }
    }
  }
  // Checks whether all blocks of the map within the bounds are empty on grid
  for (auto const & block : shape) {
    auto const actual_pos = SDL_Point{p.x + block.x,
                                      p.y + block.y};
    if (board.block(actual_pos.x, actual_pos.y) != Block::Empty) {
      return false;
    }
  }
  return true;
}

bool PlayerTetromino::IsPositionValid(Board const & board) const noexcept {
  return CheckCollision(board, shape(), position_);
}

bool PlayerTetromino::CanMoveLeft(Board const & board) const noexcept {
  auto p = position_;
  --p.x;
  return CheckCollision(board, shape(), p);
}

bool PlayerTetromino::CanMoveRight(Board const & board) const noexcept {
  auto p = position_;
  ++p.x;
  return CheckCollision(board, shape(), p);
}

bool PlayerTetromino::CanMoveDown(Board const & board) const noexcept {
  auto p = position_;
  ++p.y;
  return CheckCollision(board, shape(), p);
}

void PlayerTetromino::RotateLeft(Board const & board) noexcept {
  // 'O' can not be rotated nor kicked
  if (type() == Block::O) {
    return;
  }

  auto const r = tetromino_.RotateLeft();
  if (CheckCollision(board, r.shape(), position_)) {
    SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "Rotated left");
    tetromino_ = r;
    last_move_ = Movement::Rotation;
    return;
  }

  auto const kick_rotation_index = r.rotation_index();
  auto const & wall_kick_data = ((type() == Block::I)
      ? kWallKickData_I[kick_rotation_index ]
      : kWallKickData_JLSTZ[kick_rotation_index]);

  // Attempts all kicks in order
  for (auto const & offset : wall_kick_data) {
    auto const kicked_position = SDL_Point{
      position_.x - offset.x,
      position_.y - offset.y};
    if (CheckCollision(board, r.shape(), kicked_position)) {
      SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "Kicked left by %d,%d", offset.x, offset.y);
      tetromino_ = r;
      position_ = kicked_position;
      last_move_ = Movement::Kick;
      return;
    }
  }
}

void PlayerTetromino::RotateRight(Board const & board) noexcept {
  // 'O' can not be rotated nor kicked
  if (type() == Block::O) {
    return;
  }

  auto const r = tetromino_.RotateRight();
  if (CheckCollision(board, r.shape(), position_)) {
    SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "Rotated right");
    tetromino_ = r;
    last_move_ = Movement::Rotation;
    return;
  }

  auto const kick_rotation_index = tetromino_.rotation_index();
  auto const & wall_kick_data = ((type() == Block::I)
      ? kWallKickData_I[kick_rotation_index ]
      : kWallKickData_JLSTZ[kick_rotation_index]);

  // Attempts all kicks in order
  for (auto const & offset : wall_kick_data) {
    auto const kicked_position = SDL_Point{
      position_.x + offset.x,
      position_.y + offset.y};
    if (CheckCollision(board, r.shape(), kicked_position)) {
      SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "Kicked right by %d,%d", offset.x, offset.y);
      tetromino_ = r;
      position_ = kicked_position;
      last_move_ = Movement::Kick;
      return;
    }
  }
}

} // namespace tajga
