#include "tetromino.h"

#include "board.h"

namespace {
using tajga::Block;
using tajga::Tetromino;
using Shape = tajga::Tetromino::Shape;
using Rotations = tajga::Tetromino::Rotations;

constexpr int BlockIndexFromShape_iter(char const * map, size_t block_number, int step) {
  return (
    (block_number == 0)
    ? (step - 1)
    : (
      (*(map + step) == 'o')
      ? BlockIndexFromShape_iter(map, block_number - 1, step + 1)
      : BlockIndexFromShape_iter(map, block_number, step + 1)
      )
    );
}
template <size_t block_number>
constexpr int BlockIndexFromShape(char const * map) {
  static_assert (block_number > 0 && block_number <= 4, "Tetromino has 4 blocks");

  return BlockIndexFromShape_iter(map, block_number, 0);
}
constexpr SDL_Point CoordsFromIndex(int index) {
  return SDL_Point{index % 4, index / 4};
}
constexpr Shape BuildShape(char const * map) {
  return Shape{
    CoordsFromIndex(BlockIndexFromShape<1>(map)),
    CoordsFromIndex(BlockIndexFromShape<2>(map)),
    CoordsFromIndex(BlockIndexFromShape<3>(map)),
    CoordsFromIndex(BlockIndexFromShape<4>(map))
  };
}

const std::array<Rotations, static_cast<size_t>(Block::Count)> kTetrominos = {
  // Block::I
  Rotations{
    BuildShape("...."
               "oooo"
               "...."
               "...."),
    BuildShape("..o."
               "..o."
               "..o."
               "..o."),
    BuildShape("...."
               "...."
               "oooo"
               "...."),
    BuildShape(".o.."
               ".o.."
               ".o.."
               ".o..")},
  // Block::J
  Rotations{
    BuildShape("o..."
               "ooo."
               "...."
               "...."),
    BuildShape(".oo."
               ".o.."
               ".o.."
               "...."),
    BuildShape("...."
               "ooo."
               "..o."
               "...."),
    BuildShape(".o.."
               ".o.."
               "oo.."
               "....")},
  // Block::L
  Rotations{
    BuildShape("..o."
               "ooo."
               "...."
               "...."),
    BuildShape(".o.."
               ".o.."
               ".oo."
               "...."),
    BuildShape("...."
               "ooo."
               "o..."
               "...."),
    BuildShape("oo.."
               ".o.."
               ".o.."
               "....")},
  // Block::O
  Rotations{
    BuildShape(".oo."
               ".oo."
               "...."
               "....")},
  // Block::S
  Rotations{
    BuildShape(".oo."
               "oo.."
               "...."
               "...."),
    BuildShape(".o.."
               ".oo."
               "..o."
               "...."),
    BuildShape("...."
               ".oo."
               "oo.."
               "...."),
    BuildShape("o..."
               "oo.."
               ".o.."
               "....")},
  // Block::T
  Rotations{
    BuildShape(".o.."
               "ooo."
               "...."
               "...."),
    BuildShape(".o.."
               ".oo."
               ".o.."
               "...."),
    BuildShape("...."
               "ooo."
               ".o.."
               "...."),
    BuildShape(".o.."
               "oo.."
               ".o.."
               "....")},
  // Block::Z
  Rotations{
    BuildShape("oo.."
               ".oo."
               "...."
               "...."),
    BuildShape("..o."
               ".oo."
               ".o.."
               "...."),
    BuildShape("...."
               "oo.."
               ".oo."
               "...."),
    BuildShape(".o.."
               "oo.."
               "o..."
               "....")}
};

} // namespace

namespace tajga {

Tetromino::Tetromino(Block type, size_t rotation) noexcept
    : type_(type),
      rotation_(rotation) {
}

Rotations const & Tetromino::rotations() const noexcept {
  return kTetrominos[static_cast<size_t>(type_)];
}

Tetromino Tetromino::RotateLeft() const noexcept {
  return Tetromino(type_, ((rotation_ == 0)
                           ? (rotations().size() - 1)
                           : (rotation_ - 1)));
}

Tetromino Tetromino::RotateRight() const noexcept {
  return Tetromino(type_, ((rotation_ + 1 >= rotations().size())
                           ? 0
                           : (rotation_ + 1)));
}

} // namespace tajga
