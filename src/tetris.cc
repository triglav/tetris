#include "tetris.h"

#include <SDL2/SDL_log.h>

namespace {

// NOTE: initial position is below the buffer.
// If it is not possible to spawn the tetromino here, it will spawn within the buffer. (row 0 or 1).
SDL_Point initial_position(tajga::Board const & board) {
  return SDL_Point{board.width() / 2 - tajga::Tetromino::kSize / 2, board.buffer_height()};
}

} // namespace

// TODO: Move input stuff into a separate file
TogglableAction  move_left = TogglableAction::kNone;
TogglableAction move_right = TogglableAction::kNone;
bool rotate_left = false;
bool rotate_right = false;
bool hard_drop = false;
TogglableAction soft_drop = TogglableAction::kNone;
bool hold_piece = false;
bool pause = false;

namespace tajga {

Tetris::Tetris(Config const & config, SDL_Renderer * renderer)
    : config_(config),
      board_(config),
      current_(CreateRandomTetromino(), initial_position(board_)),
      next_{CreateRandomTetromino(), CreateRandomTetromino(), CreateRandomTetromino()},
      display_ghost_(config.display_ghost()),
      ghost_(ProjectGhost(current_)),
      level_(config.starting_level()),
      fall_throttle_(static_cast<float>(level_), config.ticks_per_second()),
      soft_drop_throttle_(config.soft_drop_speed(), config.ticks_per_second()),
      left_(config.move_speed_first(), config.move_speed_rest(), config.ticks_per_second()),
      right_(config.move_speed_first(), config.move_speed_rest(), config.ticks_per_second()),
      board_view_(board_, config.board_position(), config),
      font_(TTF_OpenFont_s(config.text_font(), config.text_size())),
      small_font_(TTF_OpenFont_s(config.small_text_font(), config.small_text_size())),
      paused_text_(font_.get(), renderer, "Paused"),
      game_over_text_(font_.get(), renderer, "Game Over"),

      next_label_(small_font_.get(), renderer, "Next"),
      hold_label_(small_font_.get(), renderer, "Hold"),

      score_label_(small_font_.get(), renderer, "Score:"),
      score_text_ (small_font_.get(), renderer, "0"),
      level_label_(small_font_.get(), renderer, "Level:"),
      level_text_ (small_font_.get(), renderer, "0"),
      lines_label_(small_font_.get(), renderer, "Lines:"),
      lines_text_ (small_font_.get(), renderer, "0") {
  SDL_assert_release(level_ > 0);
  if (level_ > 1) {
    SDL_Log("Starting on level %d", level_);
  }
}

Tetromino Tetris::CreateRandomTetromino() noexcept {
  auto const type = generator_.MakeNext();
  // Tetrominos are to always spawn with their flat side pointed down
  auto const rotation = 0;
  return Tetromino(type, rotation);
}

/** Spawns the next player tetromino
 * The tetromino initially spawns one row lower, if it can not fit, spawn it at the very top.
 * If it does not fit there either, it's game over.
 */
PlayerTetromino Tetris::SpawnNewTetromino(Tetromino tetromino) noexcept {
  auto spawn = PlayerTetromino(tetromino, initial_position(board_));
  SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "Spawning '%s' at [%d,%d]",
               to_string(spawn.type()), spawn.position().x, spawn.position().y);

  if (!spawn.AdjustSpawnPosition(board_)) {
    state_ = State::GameOver;
    SDL_Log("GAME OVER (no spot for the new tetromino)");
  }
  return spawn;
}

void Tetris::SpawnNewTetrominoAfterLockIn() noexcept {
  current_ = SpawnNewTetromino(next_.front());
  next_.pop_front();

  next_.push_back(CreateRandomTetromino());
  ghost_ = ProjectGhost(current_);
  is_current_piece_from_hold_box_ = false;
}

template<int c0, int c1, int c2, int c3, int c4>
constexpr int score_per_clear_by_level(int level, int clears_count) {
  switch (clears_count) {
  case 1: return c1 * level;
  case 2: return c2 * level;
  case 3: return c3 * level;
  case 4: return c4 * level;
  }
  return c0 * level;
}

int Tetris::CalculateScore(bool is_t_spin, int new_clears) noexcept {
  if (is_t_spin) {
    SDL_assert_paranoid(current_.last_move() == PlayerTetromino::Movement::Rotation ||
                        current_.last_move() == PlayerTetromino::Movement::Kick);
    if (config_.enable_t_spin() && current_.last_move() == PlayerTetromino::Movement::Rotation) {
      // T-Spin
      SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "T-Spin! (%d)", new_clears);
      SDL_assert(new_clears < 4);
      auto t_spin_score = score_per_clear_by_level<400, 800, 1200, 1600, 0>(level_, new_clears);
      if (config_.enable_back_to_back() && was_last_clear_hard_ && new_clears > 0) {
        SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "Back-to-Back");
        t_spin_score += t_spin_score / 2;
      }
      was_last_clear_hard_ = new_clears > 0;
      return t_spin_score ;
    }
    if (config_.enable_t_spin_mini()) {
      // T-Spin Mini
      SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "T-Spin Mini! (%d)", new_clears);
      SDL_assert(new_clears < 4);
      was_last_clear_hard_ = false;
      return score_per_clear_by_level<100, 200, 1200, 1600, 0>(level_, new_clears);
    }
  }

  auto clears_score =  score_per_clear_by_level<0, 100, 300, 500, 800>(level_, new_clears);
  if (new_clears == 4) {
    SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "TETRIS!");
    if (config_.enable_back_to_back() && was_last_clear_hard_) {
      SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "Back-to-Back");
      clears_score += clears_score/2;
    }
    was_last_clear_hard_ = true;
  } else if (new_clears > 0) {
    was_last_clear_hard_ = false;
  }
  return clears_score;
}

bool Tetris::IsOccupied(int x, int y) const noexcept {
  auto const is_within_the_board = x >= 0 && x < board_.width() && y >= 0 && y < board_.height();
  auto const is_non_empty_block = is_within_the_board && board_.block(x, y) != Block::Empty;

  if (!is_within_the_board) {
    return config_.is_border_considered_occupied();
  }
  return is_non_empty_block;
}

bool Tetris::CheckForTSpin() const noexcept {
  if (current_.type() != Block::T) {
    return false;
  }
  if (current_.last_move() != PlayerTetromino::Movement::Rotation &&
      current_.last_move() != PlayerTetromino::Movement::Kick) {
    return false;
  }
  // The 'T' centre is always at the tetromino position [x+1,y+1]
  int occupied_corner_count = 0;
  occupied_corner_count += IsOccupied(current_.position().x, current_.position().y);
  occupied_corner_count += IsOccupied(current_.position().x+2, current_.position().y);
  occupied_corner_count += IsOccupied(current_.position().x, current_.position().y+2);
  occupied_corner_count += IsOccupied(current_.position().x+2, current_.position().y+2);
  return occupied_corner_count >= 3;
}

void Tetris::LockInCurrentTetromino() noexcept {
  if (state_ != State::GameRunning) {
    return;
  }

  // We need to check for a T-Spin before we absorb and clear rows
  auto const is_t_spin = CheckForTSpin();

  board_.Absorb(current_);

  // If there are any clears, considers increasing the level
  if (!board_.rows_to_clear().empty()) {
    clears_ += board_.rows_to_clear().size();
    combo_ += 1;
    line_clear_delay_.Reset();
    if (level_ * config_.clears_per_level() <= clears_) {
      level_ += 1;
      fall_throttle_.Reset(static_cast<float>(level_));
    }
    SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "level: %d clears: %d", level_, clears_);
  } else {
    if (config_.enable_combo() && combo_ > 0) {
      auto const combo_score = combo_ * 50 * level_;
      score_ += combo_score;
      SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "COMBO: %d (%d)", score_, combo_score);
    }
    combo_ = -1;
  }

  auto const new_score = CalculateScore(is_t_spin, board_.rows_to_clear().size());
  if (new_score > 0) {
    score_ += new_score;
    SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "score: %d (%d)", score_, new_score);
  }

  // Checks whether the whole tetromino is locked within the buffer zone
  if (std::all_of(begin(current_.shape()), end(current_.shape()),
                  [y = current_.position().y, h = board_.buffer_height()]
                  (auto const & block) {
                    return y + block.y < h; })) {
    state_ = State::GameOver;
    SDL_Log("GAME OVER (locked fully within the buffer zone)");
    return;
  }

  if (board_.rows_to_clear().empty()) {
    SpawnNewTetrominoAfterLockIn();
  }
  fall_throttle_.Reset();
  lock_delay_.Reset();
}

void Tetris::Update() noexcept {
  // Do not update the logic if it's game over
  if (state_ == State::GameOver) {
    return;
  }
  if (state_ == State::GamePaused) {
    if (pause) {
      state_ = State::GameRunning;
      SDL_Log("Resumed!");
    }
    return;
  }
  SDL_assert(state_ == State::GameRunning);

  if (pause) {
    state_ = State::GamePaused;
    SDL_Log("Paused!");
    return;
  }

  if (!board_.rows_to_clear().empty()) {
    line_clear_delay_.Step();
    if (line_clear_delay_.is_done()) {
      board_.ClearRows();

      SpawnNewTetrominoAfterLockIn();
    }
    return;
  }

  // Resets the gravity state/counter upon soft_drop release - otherwise the tetromino would do the first
  // natural fall irregularly
  if (soft_drop == TogglableAction::kEnd) {
    fall_throttle_.Reset();
  }

  if (hold_piece && !is_current_piece_from_hold_box_) {
    auto const to_hold = Tetromino(current_.type(), 0);
    if (hold_box_.has_value()) {
      SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "Swapping the current '%s' with the held '%s'",
                   to_string(current_.type()), to_string(hold_box_.value().type()));
      current_ = SpawnNewTetromino(std::move(hold_box_.value()));
    } else {
      SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "Holding '%s'", to_string(current_.type()));
      current_ = SpawnNewTetromino(next_.front());
      next_.pop_front();

      next_.push_back(CreateRandomTetromino());
    }
    hold_box_.emplace(to_hold);
    is_current_piece_from_hold_box_ = true;
  } else if (hard_drop) {
    // Drops the current block down
    auto const prev_position = current_.position();
    //current_.DropAllTheWayDown(board_);
    int hard_drop_score = 0;
    while (current_.FallDown(board_)) {
      hard_drop_score += config_.hard_drop_per_cell_score();
    }
    score_ += hard_drop_score;
    SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "Hard drop lock [%d,%d] -> [%d,%d]",
                 prev_position.x, prev_position.y,
                 current_.position().x, current_.position().y);
    if (hard_drop_score > 0) {
      SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "Hard drop: %d (%d)", score_, hard_drop_score);
    }
    LockInCurrentTetromino();
  } else if (IsActive(soft_drop)) {
    if (!current_.CanMoveDown(board_)) {
      if (soft_drop == TogglableAction::kStart) {
        SDL_LogVerbose(SDL_LOG_CATEGORY_APPLICATION, "Soft drop lock [%d,%d]",
                       current_.position().x, current_.position().y);
        LockInCurrentTetromino();
        return;
      }
      lock_delay_.Step();
      if (lock_delay_.is_done()) {
        SDL_LogVerbose(SDL_LOG_CATEGORY_APPLICATION, "Lock delay ran out [%d,%d]",
                       current_.position().x, current_.position().y);
        LockInCurrentTetromino();
        return;
      }
    } else {
      soft_drop_throttle_.Call([this] {
        if (current_.FallDown(board_)) {
          score_ += config_.soft_drop_per_cell_score();
          SDL_LogVerbose(SDL_LOG_CATEGORY_APPLICATION, "Soft drop [%d,%d]",
                         current_.position().x, current_.position().y);
          soft_drop = TogglableAction::kOngoing;
        }
      });
    }
  } else {
    // Lock-in process
    if (!current_.CanMoveDown(board_)) {
      lock_delay_.Step();
      if (lock_delay_.is_done()) {
        SDL_LogVerbose(SDL_LOG_CATEGORY_APPLICATION, "Lock delay ran out [%d,%d]",
                       current_.position().x, current_.position().y);
        LockInCurrentTetromino();
        return;
      }
    } else {
      lock_delay_.Reset();
      // Natural fall of the player controlled block
      fall_throttle_.Call([this] {
        if (current_.FallDown(board_)) {
          SDL_LogVerbose(SDL_LOG_CATEGORY_APPLICATION, "Natural fall [%d,%d]",
                         current_.position().x, current_.position().y);
        }
      });
    }
  }

  if (rotate_left != rotate_right) {
    if (rotate_left) {
      current_.RotateLeft(board_);
    } else if (rotate_right) {
      current_.RotateRight(board_);
    }
    lock_delay_.Reset();
  }

  // Precomputing these allows 'DAS Interruption' - once the side wall is hit, it is possible to bounce back
  // once by holding the other move key
  auto const l = IsActive(move_left) && current_.CanMoveLeft(board_);
  auto const r = IsActive(move_right) && current_.CanMoveRight(board_);

  // Does not move if both right and left actions are pressed at the same time
  if (l && !r) {
    left_.Call([this] {
      if (current_.ShiftLeft(board_)) {
        SDL_LogVerbose(SDL_LOG_CATEGORY_APPLICATION, "Moved left [%d,%d]",
                       current_.position().x, current_.position().y);
        lock_delay_.Reset();
      }
    });
  } else if (move_left == TogglableAction::kEnd) {
    left_.Reset();
  }
  if (r && !l) {
    right_.Call([this] {
      if (current_.ShiftRight(board_)) {
        SDL_LogVerbose(SDL_LOG_CATEGORY_APPLICATION, "Moved right [%d,%d]",
                       current_.position().x, current_.position().y);
        lock_delay_.Reset();
      }
    });
  } else if (move_right == TogglableAction::kEnd) {
    right_.Reset();
  }

  ghost_ = ProjectGhost(current_);
}

PlayerTetromino Tetris::ProjectGhost(PlayerTetromino const & piece) const noexcept {
  auto ghost = piece;
  ghost.DropAllTheWayDown(board_);
  return ghost;
}

void Tetris::Draw(SDL_Renderer * renderer, double dt) noexcept {
  // Stores the last delta time, used while the game is paused
  if (state_ == State::GameRunning) {
    paused_dt_ = dt;
  } else {
    dt = paused_dt_;
  }

  board_view_.DrawBoard(renderer);
  if (display_ghost_ &&
      ghost_.position().y != current_.position().y) {
    board_view_.DrawGhostTetromino(renderer, ghost_);
  }
  if (board_.rows_to_clear().empty()) {
    board_view_.DrawPlayerTetromino(renderer, current_);

    if (!lock_delay_.is_reset()) {
      SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
      SDL_SetRenderDrawColor(renderer, 30, 30, 30, lock_delay_.Calculate<Uint8>(dt));
      board_view_.DrawTetrominoWithinBoard(renderer, current_);
      SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_NONE);
    }
  }

  SDL_Point const next_pos{300, 20};
  next_label_.Render(renderer, next_pos.x, next_pos.y);
  SDL_assert(next_.size() == 3);
  {
    int y = next_pos.y+35;
    for (auto const & n : next_) {
      board_view_.DrawTetromino(renderer, n, next_pos.x, y);
      y += 75;
    }
  }

  SDL_Point const hold_pos{300, 280};
  hold_label_.Render(renderer, hold_pos.x, hold_pos.y);
  if (hold_box_.has_value()) {
    board_view_.DrawTetromino(renderer, hold_box_.value(), hold_pos.x, hold_pos.y+35);
  }

  SDL_Point const text_pos{300, 420};

  score_label_.Render(renderer, text_pos.x, text_pos.y);
  score_text_.ChangeText(std::to_string(score_));
  score_text_.Render(renderer, text_pos.x+70, text_pos.y);

  level_label_.Render(renderer, text_pos.x, text_pos.y+25);
  level_text_.ChangeText(std::to_string(level_));
  level_text_.Render(renderer, text_pos.x+70, text_pos.y+25);

  lines_label_.Render(renderer, text_pos.x, text_pos.y+50);
  lines_text_.ChangeText(std::to_string(clears_));
  lines_text_.Render(renderer, text_pos.x+70, text_pos.y+50);

  // 'Animate' row clear
  if (!board_.rows_to_clear().empty()) {
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_ADD);
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, line_clear_delay_.Calculate<Uint8>(dt));
    for (auto y : board_.rows_to_clear()) {
      board_view_.DrawBlockLine(renderer, y);
    }
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_NONE);
  }

  // Draws "dark" overlay if the game is paused
  if (state_ == State::GamePaused ||
      state_ == State::GameOver) {
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 150);

    SDL_Rect r;
    r.x = 0;
    r.y = 0;
    SDL_GetRendererOutputSize(renderer, &r.w, &r.h);

    SDL_RenderFillRect(renderer, &r);
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_NONE);

    if (state_ == State::GamePaused) {
      paused_text_.RenderCenter(renderer, r);
    } else {
      SDL_assert(state_ == State::GameOver);
      game_over_text_.RenderCenter(renderer, r);
    }
  }
}

} // namespace tajga
