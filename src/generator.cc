#include "generator.h"

#include <algorithm>

namespace {

static int const kBlockCount = static_cast<int>(tajga::Block::Count);

constexpr bool CanBeFirst(tajga::Block block) noexcept {
  return block != tajga::Block::O &&
         block != tajga::Block::S &&
         block != tajga::Block::Z;
}

} // namespace

namespace tajga {

Generator::Generator() noexcept
    : rng_(std::random_device{}()) {
  AddNewBag();
  AdjustFirst();
}

Block Generator::MakeNext() noexcept {
  if (queue_.empty()) {
    AddNewBag();
  }
  auto const next = queue_.front();
  queue_.pop_front();
  return next;
}

void Generator::AddNewBag() noexcept {
  static_assert(kBlockCount == 7, "");
  queue_.emplace_back(Block::I);
  queue_.emplace_back(Block::J);
  queue_.emplace_back(Block::L);
  queue_.emplace_back(Block::O);
  queue_.emplace_back(Block::S);
  queue_.emplace_back(Block::T);
  queue_.emplace_back(Block::Z);
  std::shuffle(end(queue_) - kBlockCount, end(queue_), rng_);
}

void Generator::AdjustFirst() noexcept {
  while (!CanBeFirst(queue_.front())) {
    std::rotate(begin(queue_), begin(queue_) + 1, end(queue_));
  }
}

} // namespace tajga
