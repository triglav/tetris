#include <cstdio>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "auto_sdl.h"
#include "config.h"
#include "script.h"
#include "tetris.h"

namespace {

static auto running = true;
}

void PumpEvents(tajga::Tetris * tetris) {
  SDL_Event event;
  Reset(&move_left);
  Reset(&move_right);
  rotate_left = false;
  rotate_right = false;
  hard_drop = false;
  Reset(&soft_drop);
  hold_piece = false;
  pause = false;

  while (SDL_PollEvent(&event)) {
    switch (event.type) {
    case SDL_QUIT:
      running = false;
      break;
    case SDL_WINDOWEVENT:
      switch (event.window.event) {
      case SDL_WINDOWEVENT_FOCUS_LOST:
        if (tetris->state() == tajga::Tetris::State::GameRunning) {
          tetris->ChangeState(tajga::Tetris::State::GamePaused);
          SDL_Log("Paused!");
        }
        break;
      }
      break;
    case SDL_KEYDOWN:
      switch (event.key.keysym.scancode) {
      case SDL_SCANCODE_ESCAPE:
        running = false;
        break;
      case SDL_SCANCODE_LEFT:
        move_left = ((event.key.repeat == 0)
                     ? TogglableAction::kStart
                     : TogglableAction::kOngoing);
        break;
      case SDL_SCANCODE_RIGHT:
        move_right = ((event.key.repeat == 0)
                      ? TogglableAction::kStart
                      : TogglableAction::kOngoing);
        break;
      case SDL_SCANCODE_DOWN:
        soft_drop = ((event.key.repeat == 0)
                     ? TogglableAction::kStart
                     : TogglableAction::kOngoing);
        break;
      case SDL_SCANCODE_X:
      case SDL_SCANCODE_UP:
      case SDL_SCANCODE_RETURN:
        rotate_right = (event.key.repeat == 0);
        break;
      case SDL_SCANCODE_Z:
        rotate_left = (event.key.repeat == 0);
        break;
      case SDL_SCANCODE_SPACE:
        hard_drop = (event.key.repeat == 0);
        break;
      case SDL_SCANCODE_C:
        hold_piece = (event.key.repeat == 0);
        break;
      case SDL_SCANCODE_P:
        pause = (event.key.repeat == 0);
        break;
      default:
        break;
      }
      break;
    case SDL_KEYUP:
      switch (event.key.keysym.scancode) {
      case SDL_SCANCODE_LEFT:
        move_left = TogglableAction::kEnd;
        break;
      case SDL_SCANCODE_RIGHT:
        move_right = TogglableAction::kEnd;
        break;
      case SDL_SCANCODE_DOWN:
        soft_drop = TogglableAction::kEnd;
        break;
      default:
        break;
      }
      break;
    }
  }
}

int main(int, char * []) {
  if (SDL_Init(SDL_INIT_TIMER | SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0) {
    fprintf(stderr, "Failed to initalize SDL: %s\n", SDL_GetError());
    return 1;
  }
  atexit(SDL_Quit);
  if (TTF_Init() != 0) {
    fprintf(stderr, "Failed to initalize SDL TTF: %s\n", TTF_GetError());
    return 1;
  }
  atexit(TTF_Quit);

  try {
    SDL_LogSetPriority(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_INFO);

    tajga::ScriptEngine script;
    script.DoFile("config.lua");

    tajga::Config config{script};

    auto window = tajga::SDL_CreateWindow_s(
        config.window_title(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        config.window_width(), config.window_height(), SDL_WINDOW_SHOWN);

    auto renderer = tajga::SDL_CreateRenderer_s(
        window.get(), -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    tajga::Tetris tetris{config, renderer.get()};

    auto const kSkipTicks = 1000 / config.ticks_per_second();
    if (1000 % config.ticks_per_second() != 0) {
      throw std::runtime_error("(1000 / kTicksPerSecond) should result in zero");
    }
    auto const kMaxFrameskip = config.ticks_per_second() / 5;

    auto next_tick = SDL_GetTicks();
    while (running) {
      auto loops = 0;
      while (SDL_GetTicks() > next_tick && loops < kMaxFrameskip) {
        PumpEvents(&tetris);
        tetris.Update();

        next_tick += kSkipTicks;
        ++loops;
      }
      auto const dt = static_cast<double>(SDL_GetTicks() + kSkipTicks - next_tick) / kSkipTicks;
      // Clears screen
      SDL_SetRenderDrawColor(renderer.get(), 0, 0, 0, 255);
      SDL_RenderClear(renderer.get());

      tetris.Draw(renderer.get(), dt);

      SDL_RenderPresent(renderer.get());
    }
    SDL_Log("Quitting...");
  } catch (std::exception & ex) {
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "%s", ex.what());
  } catch (...) {
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Unexpected exception");
  }
  return 0;
}
