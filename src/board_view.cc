#include "board_view.h"

#include <SDL2/SDL_assert.h>

#include "player_tetromino.h"
#include "tetromino.h"

//#define TAJGA_DEBUG_RECTANGLES 1

namespace {

// I, J, L, O, S, T, Z,
SDL_Color colors[static_cast<int>(tajga::Block::Count)] = {
    {0, 150, 0},   // I - Green
    {255, 161, 0}, // J - Orange
    {0, 255, 255}, // L - Cyan
    {170, 0, 255}, // O - Purple
    {255, 255, 0}, // S - Yellow
    {255, 0, 0},   // T - Red
    {50, 0, 255},  // Z - Blue
};

void SetRenderDrawColor(SDL_Renderer * renderer, tajga::Block block, Uint8 alpha = 255) {
  SDL_assert(block != tajga::Block::Empty);
  SDL_assert(block != tajga::Block::Count);

  auto const & c = colors[static_cast<int>(block)];
  SDL_SetRenderDrawColor(renderer, c.r, c.g, c.b, alpha);
}

} // namespace

namespace tajga {

BoardView::BoardView(Board const & board, SDL_Point position, Config const & config)
  : position_(position),
    block_gap_(config.block_gap()),
    block_size_(config.block_size()),
    display_buffer_(config.display_buffer()),
    board_(board) {
}

// Draws all blocks
void BoardView::DrawBoard(SDL_Renderer * renderer) const noexcept {
  if (display_buffer_) {
    for (auto y = 0; y < board_.buffer_height(); ++y) {
      for (auto x = 0; x < board_.width(); ++x) {
        auto b = board_.block(x, y);
        if (b != Block::Empty) {
          SetRenderDrawColor(renderer, b);
          auto const r = block_rectangle(x, y);
          SDL_RenderFillRect(renderer, &r);
        }
      }
    }
  }
#if defined(TAJGA_DEBUG_RECTANGLES)
  SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
  DrawDebugRectangleAroundTiles(renderer, position_.x, position_.y,
                                board_.width(), board_.height() - board_.buffer_height());
  DrawDebugRectangleAroundTiles(renderer, 540 - position_.x, position_.y,
                                board_.width(), board_.height() - board_.buffer_height());
#endif
  for (auto y = board_.buffer_height(); y < board_.height(); ++y) {
    for (auto x = 0; x < board_.width(); ++x) {
      auto b = board_.block(x, y);
      if (b == Block::Empty) {
        SDL_SetRenderDrawColor(renderer, 50, 50, 50, 255);
      } else {
        SetRenderDrawColor(renderer, b);
      }
      auto const r = block_rectangle(x, y);
      SDL_RenderFillRect(renderer, &r);
    }
  }
}

void BoardView::DrawTetromino(SDL_Renderer * renderer,
                              Tetromino const & tetromino,
                              int x,
                              int y) const noexcept {
#if defined(TAJGA_DEBUG_RECTANGLES)
  SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
  DrawDebugRectangleAroundTiles(renderer, x, y, 4, 2);
#endif

  SetRenderDrawColor(renderer, tetromino.type());
  for (auto const & block : tetromino.shape()) {
    DrawBlock(renderer, block, {x, y});
  }
}

void BoardView::DrawPlayerTetromino(SDL_Renderer * renderer,
                                    PlayerTetromino const & tetromino) const noexcept {
  SetRenderDrawColor(renderer, tetromino.type());
  DrawTetrominoWithinBoard(renderer, tetromino);
}

void BoardView::DrawGhostTetromino(SDL_Renderer * renderer, PlayerTetromino const & ghost) const noexcept {
  SDL_SetRenderDrawColor(renderer, 30, 30, 30, 255);
  DrawTetrominoWithinBoard(renderer, ghost);
}

void BoardView::DrawBlockLine(SDL_Renderer * renderer, int row) const noexcept {
  SDL_assert(row >= board_.buffer_height() && row < board_.height());
  for (auto x = 0; x < board_.width(); ++x) {
    auto const r = block_rectangle(x, row);
    SDL_RenderFillRect(renderer, &r);
  }
}

void BoardView::DrawTetrominoWithinBoard(SDL_Renderer * renderer,
                                         PlayerTetromino const & tetromino) const noexcept {
  // Draws only blocks below the buffer zone
  for (auto const & block : tetromino.shape()) {
    if (display_buffer_ ||
        tetromino.position().y + block.y >= board_.buffer_height()) {
      DrawBlock(renderer, block, block_position(tetromino.position().x, tetromino.position().y));
    }
  }
}

void BoardView::DrawBlock(SDL_Renderer * renderer,
                          SDL_Point const & block,
                          SDL_Point const & position) const noexcept {
  auto const shift = block_size_ + block_gap_;
  auto const r = SDL_Rect{
      position.x + block.x * shift,
      position.y + block.y * shift,
      block_size_,
      block_size_
  };
  SDL_RenderFillRect(renderer, &r);
}

void BoardView::DrawDebugRectangleAroundTiles(SDL_Renderer * renderer, int x, int y, int w, int h) const noexcept {
  SDL_Rect r{x-1, y-1,
    w*block_size_ + (w-1)*block_gap_ + 2,
    h*block_size_ + (h-1)*block_gap_ + 2};
  SDL_RenderDrawRect(renderer, &r);
}

} // namespace tajga
