#include "board.h"

#include <SDL2/SDL_assert.h>
#include <SDL2/SDL_log.h>

#include "player_tetromino.h"

namespace tajga {

Board::Board(Config const & config)
: width_(config.board_width()),
  height_(config.board_height()),
  buffer_height_(config.board_buffer_height()) {
}

void Board::Absorb(PlayerTetromino const & tetromino) noexcept {
  SDL_assert(rows_to_clear_.empty());
  SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "Absorbing '%s' at [%d,%d]",
               to_string(tetromino.type()), tetromino.position().x, tetromino.position().y);

  // Copies the tetromino blocks to the board
  for (auto const & block : tetromino.shape()) {
    auto & destination = grid_[index(tetromino.position().x + block.x,
                                     tetromino.position().y + block.y)];
    SDL_assert(destination == Block::Empty);
    destination = tetromino.type();
  }
  // Checks for rows to clear
  auto const top = std::max(tetromino.position().y, 0);
  auto const bottom = std::min(tetromino.position().y + Tetromino::kSize,
                               height_);
  for (int y = top; y < bottom; ++y) {
    if (IsRowFull(y)) {
      rows_to_clear_.push_back(y);
    }
  }
}

void Board::ClearRows() noexcept {
  SDL_assert(!rows_to_clear_.empty());
  for (auto y : rows_to_clear_) {
    SDL_assert_paranoid(IsRowFull(y));
    auto const first = begin(grid_) + index(0, y);
    auto const last = first + width_;
    SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "Clearing row #%d", height_ - y);

    // If not, removes the row
    std::move_backward(begin(grid_), first, last);
    std::fill_n(begin(grid_), width_, Block::Empty);
  }
  rows_to_clear_.clear();
  SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "Cleared %d rows", rows_to_clear_.size());
}

bool Board::IsRowFull(int y) const noexcept {
  SDL_assert(y >= 0 && y < height_);
  auto const first = begin(grid_) + index(0, y);
  auto const last = first + width_;

  auto gap = std::find(first, last, Block::Empty);
  return gap == last;
}

} // namespace tajga
