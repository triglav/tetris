#include "config.h"

#include "script.h"

namespace tajga {

Config::Config(ScriptEngine const & script)
  : script_(script),
    // system
    ticks_per_second_    (script_.get<int>("system.ticks_per_second")),
    // window
    window_title_        (script_.get<std::string>("window.title")),
    window_width_        (script_.get<int>("window.width")),
    window_height_       (script_.get<int>("window.height")),
    // text
    text_font_           (script_.get<std::string>("text.font")),
    text_size_           (script_.get<int>("text.size")),
    small_text_font_     (script_.get<std::string>("small_text.font")),
    small_text_size_     (script_.get<int>("small_text.size")),
    // tetris
    move_speed_first_    (script_.get<float>("tetris.move_speed_first")),
    move_speed_rest_     (script_.get<float>("tetris.move_speed_rest")),
    soft_drop_speed_     (script_.get<float>("tetris.soft_drop_speed")),
    // game rules
    clears_per_level_    (script_.get<int>("tetris.rules.clears_per_level")),
    enable_t_spin_       (script_.get<bool>("tetris.rules.enable_t_spin")),
    enable_t_spin_mini_  (script_.get<bool>("tetris.rules.enable_t_spin_mini")),
    is_border_considered_occupied_(script_.get<bool>("tetris.rules.is_border_considered_occupied")),
    enable_back_to_back_ (script_.get<bool>("tetris.rules.enable_back_to_back")),
    enable_combo_        (script_.get<bool>("tetris.rules.enable_combo")),
    soft_drop_per_cell_score_(script_.get<int>("tetris.rules.soft_drop_per_cell_score")),
    hard_drop_per_cell_score_(script_.get<int>("tetris.rules.hard_drop_per_cell_score")),
    // board/game
    board_width_         (script_.get<int>("tetris.board.width")),
    board_height_        (script_.get<int>("tetris.board.height")),
    board_buffer_height_ (script_.get<int>("tetris.board.buffer_height")),
    // board/game view
    board_position_      (script_.get<SDL_Point>("tetris.board.view.position")),
    block_gap_           (script_.get<int>("tetris.board.view.block_gap")),
    block_size_          (script_.get<int>("tetris.board.view.block_size")),
    display_buffer_      (script_.get<bool>("tetris.board.view.display_buffer")),
    // game options
    starting_level_      (script_.get<int>("options.starting_level")),
    display_ghost_       (script_.get<bool>("options.display_ghost")) {
}

} // namespace tajga
