#pragma once

namespace tajga {

enum class Block : char {
  First = 0,
  I = 0, J, L, O, S, T, Z,
  Count,
  Empty,
};

constexpr char const * to_string(Block b) {
  switch (b) {
  case Block::I: return "I";
  case Block::J: return "J";
  case Block::L: return "L";
  case Block::O: return "O";
  case Block::S: return "S";
  case Block::T: return "T";
  case Block::Z: return "Z";
  default: return "<unknown>";
  }
}

} // namespace tajga

