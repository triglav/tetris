#pragma once

#include <memory>

#include <SDL2/SDL_ttf.h>

#include "auto_sdl.h"
#include "sdl_exception.h"

namespace tajga {

using auto_TTF_Font = std::unique_ptr<TTF_Font, decltype(TTF_CloseFont) *>;

inline auto_TTF_Font TTF_OpenFont_s(const char * file, int ptsize) {
  auto p = TTF_OpenFont(file, ptsize);
  if (p == nullptr) {
    throw sdl_exception("Failed to open font");
  }
  return auto_TTF_Font(p, TTF_CloseFont);
}

inline auto_SDL_Surface TTF_RenderText_Shaded_s(
    TTF_Font * font, const char * text, SDL_Color fg, SDL_Color bg) {
  auto p = TTF_RenderText_Shaded(font, text, fg, bg);
  if (p == nullptr) {
    throw sdl_exception("Failed to render text");
  }
  return auto_SDL_Surface(p, SDL_FreeSurface);
}

inline auto_SDL_Surface TTF_RenderText_Blended_s(
    TTF_Font * font, const char * text, SDL_Color fg) {
  auto p = TTF_RenderText_Blended(font, text, fg);
  if (p == nullptr) {
    throw sdl_exception("Failed to render text");
  }
  return auto_SDL_Surface(p, SDL_FreeSurface);
}

} // namespace tajga
