#pragma once

#include <stdexcept>
#include <string>

#include <lua.hpp>
#include <SDL2/SDL_assert.h>
#include <SDL2/SDL_rect.h>

struct lua_State;

namespace tajga {

class LuaError : public std::runtime_error {
public:
  explicit LuaError(lua_State * lua_state) noexcept;
}; // class LuaError

class LuaVariableError : public std::runtime_error {
public:
  LuaVariableError(std::string const & message, std::string const & identificator) noexcept;
}; // class LuaVariableError

class ScriptEngine {
public:
  ScriptEngine();
  ~ScriptEngine() noexcept;

  void DoFile(char const * file);
  void DoString(char const * str);

  template <typename T, typename std::enable_if_t<std::is_same_v<bool, T>> * = nullptr>
  T get(std::string const & identificator) const {
    LoadVariable(identificator);
    if (!lua_isboolean(lua_state_, -1)) {
      throw LuaVariableError("Boolean variable expected", identificator);
    }

    auto value = lua_toboolean(lua_state_, -1);

    lua_pop(lua_state_, 1);
    SDL_assert_paranoid(lua_gettop(lua_state_) == 0);

    return static_cast<bool>(value);
  }

  template <typename T, typename std::enable_if_t<std::is_integral_v<T> && !std::is_same_v<bool, T>> * = nullptr>
  T get(std::string const & identificator) const {
    LoadVariable(identificator);
    if (!lua_isinteger(lua_state_, -1)) {
      throw LuaVariableError("Integral variable expected", identificator);
    }

    auto value = lua_tointeger(lua_state_, -1);

    lua_pop(lua_state_, 1);
    SDL_assert_paranoid(lua_gettop(lua_state_) == 0);

    return static_cast<T>(value);
  }

  template <typename T, typename std::enable_if_t<std::is_floating_point_v<T>> * = nullptr>
  T get(std::string const & identificator) const {
    LoadVariable(identificator);
    if (!lua_isnumber(lua_state_, -1)) {
      throw LuaVariableError("Floating point variable expected", identificator);
    }

    auto value = lua_tonumber(lua_state_, -1);

    lua_pop(lua_state_, 1);
    SDL_assert_paranoid(lua_gettop(lua_state_) == 0);

    return static_cast<float>(value);
  }

  template <typename T, typename std::enable_if_t<std::is_same_v<std::string, T>> * = nullptr>
  T get(std::string const & identificator) const {
    LoadVariable(identificator);
    if (!lua_isstring(lua_state_, -1)) {
      throw LuaVariableError("String variable expected", identificator);
    }

    size_t length;
    auto value = lua_tolstring(lua_state_, -1, &length);
    auto value_s = std::string{value, length};

    lua_pop(lua_state_, 1);
    SDL_assert_paranoid(lua_gettop(lua_state_) == 0);

    return value_s;
  }

  template <typename T, typename std::enable_if_t<std::is_same_v<SDL_Point, T>> * = nullptr>
  T get(std::string const & identificator) const {
    LoadVariable(identificator);
    if (!lua_istable(lua_state_, -1)) {
      throw LuaVariableError("SDL_Point variable expected (table with two values)", identificator);
    }

    if (luaL_len(lua_state_, -1) != 2) {
      throw LuaVariableError("SDL_Point variable expected (other than two table values found)", identificator);
    }

    // Pushes both point values to the stack
    lua_geti(lua_state_, -1, 1);
    lua_geti(lua_state_, -2, 2);

    if (!lua_isinteger(lua_state_, -2)) {
      throw LuaVariableError("Integer variable expected", identificator + ".x");
    }
    if (!lua_isinteger(lua_state_, -1)) {
      throw LuaVariableError("Integer variable expected", identificator + ".y");
    }

    auto const point = SDL_Point{
        static_cast<decltype(SDL_Point::x)>(lua_tointeger(lua_state_, -2)),
        static_cast<decltype(SDL_Point::y)>(lua_tointeger(lua_state_, -1))};

    lua_pop(lua_state_, 3);
    SDL_assert_paranoid(lua_gettop(lua_state_) == 0);

    return point;
  }

private:
  void LoadVariable(std::string const & identificator) const;

private:
  lua_State * const lua_state_;

}; // class ScriptEngine

} // namespace tajga
