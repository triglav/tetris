#pragma once

#include <deque>
#include <random>

#include "block.h"

namespace tajga {

class Generator {
public:
  Generator(Generator const &) = delete;
  Generator & operator= (Generator const &) = delete;

  Generator(Generator &&) noexcept = default;
  Generator & operator= (Generator &&) noexcept = default;

  Generator() noexcept;
  ~Generator() noexcept = default;

  /// Generates new Tetromino.
  Block MakeNext() noexcept;

private:
  /// Adds a new bag of 7 to the queue.
  void AddNewBag() noexcept;
  /// Changes the first Tetromino, if it's a non-desired one
  void AdjustFirst() noexcept;

private:
  std::mt19937 rng_;
  std::deque<Block> queue_;

}; // class Generator

} // namespace tajga
