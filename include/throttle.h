#pragma once

#include <SDL2/SDL_assert.h>
#include <functional>

namespace tajga {

/** Throttles the execution of the given action.
 * Executes the action according to the given count of calls per second.
 */
class Throttle {
public:
  Throttle() = delete;

  Throttle(Throttle const &) = delete;
  Throttle & operator= (Throttle const &) = delete;

  Throttle(Throttle &&) noexcept = default;
  Throttle & operator= (Throttle &&) noexcept = default;

  /**
   * @param calls_per_second Number of calls per second.
   * @param ticks_per_second The number of logical updates (the game loop) per second.
   */
  Throttle(float calls_per_second, int ticks_per_second) noexcept
    : calls_per_second_(calls_per_second),
      ticks_per_second_(ticks_per_second) {
    SDL_assert_release(calls_per_second >= 0.0f);
  }
  ~Throttle() noexcept = default;

  /** Throttles the execution of the given function.
   * @param callback Function to be throttled.
   */
  void Call(std::function<void()> const & callback) {
    SDL_assert(callback);
    current_ticks_ += calls_per_second_;
    while (current_ticks_ >= ticks_per_second_) {
      current_ticks_ -= ticks_per_second_;
      callback();
    }
  }

  void Reset() noexcept { current_ticks_ = 0.0f; }
  void Reset(float calls_per_second) noexcept {
    calls_per_second_ = calls_per_second;
    current_ticks_ = 0.0f;
  }

private:
  /// The number of calls of the Call() method per second.
  float calls_per_second_;
  /// The number of logical updates (the game loop) per second.
  int const ticks_per_second_;
  /// The current throttle state.
  float current_ticks_{0.0f};
}; // class Throttle

/** Delays the second execution of the given action.
 * Executes the first action right away, following by a delay calculated according to the first given speed.
 * Afterwards executes the action according to the second given speed.
 */
class DelayedThrottle {
public:
  DelayedThrottle() = delete;

  DelayedThrottle(DelayedThrottle const &) = delete;
  DelayedThrottle & operator= (DelayedThrottle const &) = delete;

  DelayedThrottle(DelayedThrottle &&) noexcept = default;
  DelayedThrottle & operator= (DelayedThrottle &&) noexcept = default;

  /**
   * @param first The initial speed for the first to second execution.
   * @param rest  The speed for remaining executions after the second.
   * @param ticks_per_second The number of logical updates (the game loop) per second.
   */
  DelayedThrottle(float first, float rest, int ticks_per_second) noexcept
    : first_(first),
      rest_(rest),
      step_(first_),
      ticks_per_second_(ticks_per_second) {
    SDL_assert_release(rest > 0.0f);
    SDL_assert_release(first < rest);
  }
  ~DelayedThrottle() noexcept = default;

  /** Throttles the execution of the given function.
   * @param callback  Function to be throttled.
   */
  void Call(std::function<void()> const & callback) {
    SDL_assert(callback);

    current_ticks_ += step_;
    if (is_first_) {
      is_first_ = false;
      callback();
    }
    while (current_ticks_ >= ticks_per_second_) {
      current_ticks_ -= ticks_per_second_;
      step_ = rest_;
      callback();
    }
  }

  void Reset() noexcept {
    step_ = first_;
    current_ticks_ = 0.0f;
    is_first_ = true;
  }

private:
  /// The initial speed for the first to second execution.
  float first_;
  /// The speed for remaining executions after the second.
  float rest_;
  /// The next step the counter is going to be increased by.
  float step_;
  /// The number of logical updates (the game loop) per second.
  int const ticks_per_second_;
  /// Current throttle state.
  float current_ticks_{0.0f};
  /// Indicates whether this execution is the first one.
  bool is_first_{true};

}; // class DelayedThrottle

} // namespace tajga
