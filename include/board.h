#pragma once

#include <vector>
#include <SDL2/SDL_assert.h>

#include "block.h"
#include "config.h"

namespace tajga {

class PlayerTetromino;

class Board {
public:
  Board() = delete;

  Board(Board const &) = delete;
  Board & operator= (Board const &) = delete;

  Board(Board &&) noexcept = default;
  Board & operator= (Board &&) noexcept = default;

  explicit Board(Config const & config);
  ~Board() noexcept = default;

  constexpr size_t index(int x, int y) const noexcept {
    SDL_assert_paranoid(x >= 0 && x < width_);
    SDL_assert_paranoid(y >= 0 && y < height_);
    return static_cast<size_t>(y * width_ + x);
  }
  Block block(int x, int y) const noexcept { return grid_[index(x, y)]; }
  int width() const noexcept { return width_; }
  int height() const noexcept { return height_; }
  constexpr int buffer_height() const noexcept { return buffer_height_; }
  std::vector<int> const & rows_to_clear() const noexcept { return rows_to_clear_; }

  /// Absorbs given tetromino into the board (its blocks become part of the board).
  /// Sets the rows that shall be cleared (full rows - without a gap).
  void Absorb(PlayerTetromino const & tetromino) noexcept;
  /// Clears rows.
  /// The rows have to be full (without a gap).
  void ClearRows() noexcept;

private:
  /// Checks if there is any gap in the row
  bool IsRowFull(int y) const noexcept;

private:
  /// Number of blocks that fit horizontally within the board
  int const width_;
  /// Number of blocks that fit vertically within the board (including the buffer)
  int const height_;
  /// Number of upper hidden rows.
  /// All tetrominoes spawn within this area.
  /// If tetromino locks within this area, it's a game over.
  int const buffer_height_;
  /// All blocks within the grid
  std::vector<Block> grid_{static_cast<size_t>(width_ * height_), Block::Empty};
  /// Indices of full rows.
  /// Filled via Absorb() and cleared via ClearRows().
  std::vector<int> rows_to_clear_{};
}; // class Board

} // namespace tajga
