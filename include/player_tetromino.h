#pragma once

#include <SDL2/SDL_assert.h>

#include "tetromino.h"

namespace tajga {

class Board;

using RotationTest = std::array<SDL_Point, 4>;
using WallKickData = std::array<RotationTest, 4>;

class PlayerTetromino {
public:
  enum class Movement : int {
    None,
    Fall,
    Shift,
    Rotation,
    Kick,
  }; // enum class Movement
public:
  PlayerTetromino() = delete;
  PlayerTetromino(PlayerTetromino const &) noexcept = default;
  PlayerTetromino & operator= (PlayerTetromino const &) noexcept = default;

  PlayerTetromino(PlayerTetromino &&) noexcept = default;
  PlayerTetromino & operator= (PlayerTetromino &&) noexcept = default;

  PlayerTetromino(Tetromino tetromino, SDL_Point position) noexcept;
  ~PlayerTetromino() noexcept = default;

public:
  /** Used to move the tetromino up within the buffer if the top row is full when the tetromino spawns.
   * @param board The current tetris board
   * @returns True if the position is valid
   */
  bool AdjustSpawnPosition(Board const & board) noexcept {
    SDL_assert(position_.y > 0);
    while (position_.y > 0) {
      if (IsPositionValid(board)) {
        return true;
      }
      position_.y -= 1;
    }
    return IsPositionValid(board);
  }
  /** Drops the tetromino down by one row.
   * @param board The current tetris board
   * @returns False if it can not be dropped
   */
  bool FallDown(Board const & board) noexcept {
    auto const b = CanMoveDown(board);
    if (b) {
      position_.y += 1;
      last_move_ = Movement::Fall;
    }
    return b;
  }
  /** Drops the tetromino all the way down to the lowest possible (free) row.
   * @param board The current tetris board
   */
  void DropAllTheWayDown(Board const & board) noexcept {
    while (CanMoveDown(board)) {
      position_.y += 1;
      last_move_ = Movement::Fall;
    }
  }
  /** Shifts the tetromino to the left by one column.
   * @param board The current tetris board
   * @returns False if it can not be shifted
   */
  bool ShiftLeft(Board const & board) noexcept {
    auto const b = CanMoveLeft(board);
    if (b) {
      position_.x -= 1;
      last_move_ = Movement::Shift;
    }
    return b;
  }
  /** Shifts the tetromino to the right by one column.
   * @param board The current tetris board
   * @returns False if it can not be shifted
   */
  bool ShiftRight(Board const & board) noexcept {
    auto const b = CanMoveRight(board);
    if (b) {
      position_.x += 1;
      last_move_ = Movement::Shift;
    }
    return b;
  }
  void RotateLeft(Board const & board) noexcept;
  void RotateRight(Board const & board) noexcept;

private:
  bool IsPositionValid(Board const & board) const noexcept;

public:
  bool CanMoveDown(Board const & board) const noexcept;
  bool CanMoveLeft(Board const & board) const noexcept;
  bool CanMoveRight(Board const & board) const noexcept;

  SDL_Point const & position() const noexcept { return position_; }
  Block type() const noexcept { return tetromino_.type(); }
  Tetromino::Shape const & shape() const noexcept { return tetromino_.shape(); }
  Movement last_move() const noexcept { return last_move_; }

private:
  Tetromino tetromino_;
  /// Position within the board
  SDL_Point position_;
  Movement last_move_{Movement::None};
}; // class PlayerTetromino

} // namespace tajga
