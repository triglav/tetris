#pragma once

#include <optional>

#include "auto_ttf.h"
#include "board.h"
#include "board_view.h"
#include "config.h"
#include "generator.h"
#include "linear_interpolator.h"
#include "player_tetromino.h"
#include "static_text.h"
#include "tetromino.h"
#include "throttle.h"

enum class TogglableAction : int {
  kNone,
  kStart,
  kOngoing,
  kEnd
}; // enum class TogglableAction

inline bool IsActive(TogglableAction a) {
  return a == TogglableAction::kStart || a == TogglableAction::kOngoing;
}
inline void Reset(TogglableAction * a) {
  if (*a == TogglableAction::kEnd) {
    *a = TogglableAction::kNone;
  }
}

extern TogglableAction move_left;
extern TogglableAction move_right;
extern bool rotate_left;
extern bool rotate_right;
extern bool hard_drop;
extern TogglableAction soft_drop;
extern bool hold_piece;
extern bool pause;

namespace tajga {

class Tetris {
public:
  enum class State {
    GameRunning,
    GamePaused,
    GameOver,
  }; // enum class State {
public:
  Tetris() = delete;

  Tetris(Tetris const &) = delete;
  Tetris & operator= (Tetris const &) = delete;

  Tetris(Tetris &&) = default;
  Tetris & operator= (Tetris &&) = default;

  Tetris(Config const & config, SDL_Renderer * renderer);
  ~Tetris() noexcept = default;

  /// Updates the game each logic frame
  void Update() noexcept;

  void Draw(SDL_Renderer * renderer, double dt) noexcept;

  void ChangeState(State state) noexcept {
    state_ = state;
  }
  State state() const noexcept { return state_; }

private:
  // Creates a new random shape/rotation tetromino
  Tetromino CreateRandomTetromino() noexcept;
  // Spawns the next player tetromino
  PlayerTetromino SpawnNewTetromino(Tetromino tetromino) noexcept;
  void SpawnNewTetrominoAfterLockIn() noexcept;
  void LockInCurrentTetromino() noexcept;
  // Determines whether the block is "occupied" (T-Spin).
  bool IsOccupied(int x, int y) const noexcept;
  bool CheckForTSpin() const noexcept;
  int CalculateScore(bool is_t_spin, int new_clears) noexcept;

  PlayerTetromino ProjectGhost(PlayerTetromino const & piece) const noexcept;

private:
  Config const & config_;
  /// Generates 'randomized' tetromino sequence
  Generator generator_;
  /// Tetris board
  Board board_;
  /// Player controlled block, starts in the middle of the board, at the top
  PlayerTetromino current_;
  /// The next tetromino to spawn
  std::deque<Tetromino> next_;

  /// Tetromino storage for the hold functionality
  std::optional<Tetromino> hold_box_{};
  /// If the current piece is from the hold box, it can not be put back in again
  bool is_current_piece_from_hold_box_{false};

  /// Indicates whether the ghost piece is displayed
  bool display_ghost_;
  /// The position of the current player tetromino if allowed to drop
  PlayerTetromino ghost_;

  /// Indicates the current game state
  State state_{State::GameRunning};

  /// The current game level
  int level_;
  /// Number of cleared rows
  int clears_{0};
  /// The current score
  int score_{0};
  /// Indicates whether the last clear was a T-Spin or Tetris (used for combo)
  bool was_last_clear_hard_{false};
  /// The current combo counter, starts always at -1
  int combo_{-1};

  /// Controls player controlled blocks fall speed
  Throttle fall_throttle_;
  Throttle soft_drop_throttle_;

  DelayedThrottle left_;
  DelayedThrottle right_;

  LinearInterpolator<int> line_clear_delay_{0, 255, 6};
  LinearInterpolator<int> lock_delay_{0, 255, 10};

  /// Displays the board to the screen
  BoardView board_view_;
  /// Used while the game is paused - so animations do not flicker with the changing 'dt'.
  double paused_dt_;
  auto_TTF_Font font_;
  auto_TTF_Font small_font_;

  StaticText paused_text_;
  StaticText game_over_text_;

  StaticText next_label_;
  StaticText hold_label_;

  StaticText score_label_;
  StaticText score_text_;
  StaticText level_label_;
  StaticText level_text_;
  StaticText lines_label_;
  StaticText lines_text_;
}; // class Tetris

} // namespace tajga
