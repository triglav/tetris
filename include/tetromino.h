#pragma once

#include <array>
#include <vector>

#include <SDL2/SDL_rect.h>

#include "block.h"

namespace tajga {

class Tetromino {
public:
  /// The dimension of block map (4x4)
  static int const kSize = 4;
  /// 4x4 block map, tetromino shape is defined by 4 blocks
  using Shape = std::array<SDL_Point, 4>;
  /// Tetromino may be rotated, blocks are then in different positions of the 4x4 block map
  using Rotations = std::vector<Shape>;

public:
  Tetromino() = delete;
  Tetromino(Tetromino const &) noexcept = default;
  Tetromino & operator=(Tetromino const &) noexcept = default;

  Tetromino(Tetromino &&) noexcept = default;
  Tetromino & operator=(Tetromino &&) noexcept = default;

  Tetromino(Block type, size_t rotation) noexcept;
  ~Tetromino() noexcept = default;

public:
  Tetromino RotateLeft() const noexcept;
  Tetromino RotateRight() const noexcept;

  Block type() const noexcept { return type_; }
  Shape const & shape() const noexcept { return rotations()[rotation_]; }
  size_t rotation_index() const noexcept { return rotation_; }

private:
  Rotations const & rotations() const noexcept;

private:
  /// Type of the tetromino, mostly used for the color
  Block type_;
  /// Current rotation, defines which block map is currently used
  size_t rotation_;
}; // class Tetromino

} // namespace tajga
