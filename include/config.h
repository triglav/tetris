#pragma once

#include <string>

#include <SDL2/SDL_rect.h>

namespace tajga {

class ScriptEngine;

// Not very nice, however I prefer "type and name control" over convenience (e.g. map/dictionary).
class Config {
public:
  Config() = delete;
  Config(Config const &) = delete;
  Config & operator= (Config const &) = delete;

  Config(Config &&) noexcept = default;
  Config & operator= (Config &&) noexcept = default;

  explicit Config(ScriptEngine const & script);
  ~Config() noexcept = default;

private:
  ScriptEngine const & script_;

/// Adds a readonly property
#define TAJGA_ADD_VARIABLE(TYPE, NAME) \
public: \
  TYPE NAME() const noexcept { return NAME##_; } \
private: \
  TYPE NAME##_

/// Adds a readonly string property
#define TAJGA_ADD_STRING(NAME) \
public: \
  char const * NAME() const noexcept { return NAME##_.c_str(); } \
private: \
  std::string NAME##_

  // system
  TAJGA_ADD_VARIABLE(int, ticks_per_second);
  // window
  TAJGA_ADD_STRING(window_title);
  TAJGA_ADD_VARIABLE(int, window_width);
  TAJGA_ADD_VARIABLE(int, window_height);
  // text
  TAJGA_ADD_STRING(text_font);
  TAJGA_ADD_VARIABLE(int, text_size);
  TAJGA_ADD_STRING(small_text_font);
  TAJGA_ADD_VARIABLE(int, small_text_size);
  // tetris
  TAJGA_ADD_VARIABLE(float, move_speed_first);
  TAJGA_ADD_VARIABLE(float, move_speed_rest);
  TAJGA_ADD_VARIABLE(float, soft_drop_speed);
  // game rules
  TAJGA_ADD_VARIABLE(int, clears_per_level);
  TAJGA_ADD_VARIABLE(bool, enable_t_spin);
  TAJGA_ADD_VARIABLE(bool, enable_t_spin_mini);
  TAJGA_ADD_VARIABLE(bool, is_border_considered_occupied);
  TAJGA_ADD_VARIABLE(bool, enable_back_to_back);
  TAJGA_ADD_VARIABLE(bool, enable_combo);
  TAJGA_ADD_VARIABLE(int, soft_drop_per_cell_score);
  TAJGA_ADD_VARIABLE(int, hard_drop_per_cell_score);
  // board/game
  TAJGA_ADD_VARIABLE(int, board_width);
  TAJGA_ADD_VARIABLE(int, board_height);
  TAJGA_ADD_VARIABLE(int, board_buffer_height);
  // board/game view
  TAJGA_ADD_VARIABLE(SDL_Point, board_position);
  TAJGA_ADD_VARIABLE(int, block_gap);
  TAJGA_ADD_VARIABLE(int, block_size);
  TAJGA_ADD_VARIABLE(bool, display_buffer);
  // game options
  TAJGA_ADD_VARIABLE(int, starting_level);
  TAJGA_ADD_VARIABLE(bool, display_ghost);

#undef TAJGA_ADD_VARIABLE
}; // class Config

} // namespace tajga
