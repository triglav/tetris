#pragma once

#include <sstream>
#include <stdexcept>
#include <string>

#include <SDL2/SDL_error.h>

namespace tajga {

class sdl_exception : public std::runtime_error {
public:
  sdl_exception(char const * msg)
      : runtime_error(FormatError(msg)) { }

private:
  static std::string FormatError(char const * msg) {
    std::stringstream ss;
    ss << msg << ": " << SDL_GetError();
    return ss.str();
  }
}; // class sdl_exception

} // namespace tajga
