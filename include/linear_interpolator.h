#pragma once

#include <algorithm>

namespace tajga {

/// Used to calculate interpolated value within the given interval based on the current time delta.
template <typename T>
class LinearInterpolator {
public:
  LinearInterpolator(LinearInterpolator const &) = delete;
  LinearInterpolator & operator= (LinearInterpolator const &) = delete;

  LinearInterpolator(LinearInterpolator &&) noexcept = default;
  LinearInterpolator & operator= (LinearInterpolator &&) noexcept = default;

  LinearInterpolator() = delete;

  /**
   * @param start The first value (inclusive) of the interval
   * @param end The last value (inclusive) of the interval
   * @param step_count Number of steps needed to pass the whole interval
   */
  LinearInterpolator(T start, T end, int step_count) noexcept
  : start_(start),
    end_(end),
    interval_start_(std::min(start, end)),
    interval_end_(std::max(start, end)),
    step_count_(step_count),
    step_(0),
    delta_(static_cast<double>(end - start) / (step_count-1)),
    current_(start) {
  }
  ~LinearInterpolator() noexcept = default;

  /// Resets the interpolator to the start of the interval.
  void Reset() noexcept {
    step_ = 0;
    current_ = start_;
  }
  /// Does a single step through the interval.
  void Step() noexcept {
    if (step_ < step_count_) {
      step_ += 1;
      current_ += delta_;
    }
  }
  /// Interpolates the current value based on the given delta time.
  template <typename T2 = T>
  T2 Calculate(double dt) const noexcept {
    auto x = static_cast<T>((current_ - delta_) * (1.0 - dt) + current_ * dt);
    return static_cast<T2>(std::clamp(x, interval_start_, interval_end_));
  }
  /// Indicates whether the interpolator has finished the interval.
  bool is_done() const noexcept { return step_ >= step_count_; }
  bool is_reset() const noexcept { return step_ == 0; }

private:
  // The first interval value.
  T const start_;
  // The last interval value.
  T const end_;
  // The mathematical start of the value interval.
  T const interval_start_;
  // The mathematical end of the value interval.
  T const interval_end_;
  // Number of steps to be taken to pass through the interval.
  int const step_count_;
  // The current step.
  int step_;
  // Amount used to increase the current value per step.
  double const delta_;
  // The current value within the interval.
  double current_;
}; // class LinearInterpolator

} // namespace tajga
