#pragma once

#include "auto_ttf.h"

namespace tajga {

/// Preloaded text texture
class StaticText {
public:
  StaticText(StaticText const &) = delete;
  StaticText & operator= (StaticText const &) = delete;

  StaticText(StaticText &&) = default;
  StaticText & operator= (StaticText &&) = default;

  StaticText() = delete;

  StaticText(TTF_Font * font, SDL_Renderer * renderer, std::string const & text, SDL_Color color = {255, 255, 255})
  : font_(font),
    renderer_(renderer),
    text_(text),
    color_(color),
    texture_(nullptr, nullptr) {
    ReloadTexture();
  }
  ~StaticText() noexcept = default;

  void ChangeText(std::string const & text) {
    if (!texture_ || text_ != text) {
      text_ = text;
      ReloadTexture();
    }
  }

  void RenderCenter(SDL_Renderer * renderer, SDL_Rect area) const noexcept {
    RenderCenter(renderer,
                 area.x + area.w/2,
                 area.y + area.h/2);
  }
  void RenderCenter(SDL_Renderer * renderer, int x, int y) const noexcept {
    Render(renderer,
           x - width_/2,
           y - height_/2);
  }
  void Render(SDL_Renderer * renderer, int x, int y) const noexcept {
    SDL_Rect dest{
        x,
        y,
        width_,
        height_
    };
    SDL_RenderCopy(renderer, texture_.get(), nullptr, &dest);
  }

private:
  void ReloadTexture() {
    auto surface = tajga::TTF_RenderText_Blended_s(font_, text_.c_str(), color_);
    texture_ = tajga::SDL_CreateTextureFromSurface_s(renderer_, surface.get());
    if (SDL_QueryTexture(texture_.get(), nullptr, nullptr, &width_, &height_) != 0) {
      throw sdl_exception("Failed to query text texture");
    }
  }

private:
  TTF_Font * font_;
  SDL_Renderer * renderer_;
  std::string text_;
  SDL_Color color_;
  auto_SDL_Texture texture_;
  int width_;
  int height_;
}; // class StaticText

} // namespace tajga
