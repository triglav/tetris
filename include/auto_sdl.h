#pragma once

#include <memory>

#include <SDL2/SDL_render.h>
#include <SDL2/SDL_video.h>

#include "sdl_exception.h"

namespace tajga {

using auto_SDL_Window = std::unique_ptr<SDL_Window, decltype(SDL_DestroyWindow) *>;
using auto_SDL_Renderer = std::unique_ptr<SDL_Renderer, decltype(SDL_DestroyRenderer) *>;
using auto_SDL_Surface = std::unique_ptr<SDL_Surface, decltype(SDL_FreeSurface) *>;
using auto_SDL_Texture = std::unique_ptr<SDL_Texture, decltype(SDL_DestroyTexture) *>;

inline auto_SDL_Window SDL_CreateWindow_s(
    const char * title, int x, int y, int w, int h, Uint32 flags) {
  auto p = SDL_CreateWindow(title, x, y, w, h, flags);
  if (p == nullptr) {
    throw sdl_exception("Failed to create window");
  }
  return auto_SDL_Window(p, SDL_DestroyWindow);
}

inline auto_SDL_Renderer SDL_CreateRenderer_s(
    SDL_Window * window, int index, Uint32 flags) {
  auto p = SDL_CreateRenderer(window, index, flags);
  if (p == nullptr) {
    throw sdl_exception("Failed to create renderer");
  }
  return auto_SDL_Renderer(p, SDL_DestroyRenderer);
}

inline auto_SDL_Texture SDL_CreateTextureFromSurface_s(
    SDL_Renderer * renderer, SDL_Surface * surface) {
  auto p = SDL_CreateTextureFromSurface(renderer, surface);
  if (p == nullptr) {
    throw sdl_exception("Failed to create texture from surface");
  }
  return auto_SDL_Texture(p, SDL_DestroyTexture);
}

} // namespace tajga
