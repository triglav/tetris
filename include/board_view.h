#pragma once

#include <SDL2/SDL_rect.h>
#include <SDL2/SDL_render.h>

#include "board.h"
#include "config.h"
#include "tetromino.h"

namespace tajga {

class PlayerTetromino;

class BoardView {
public:
  BoardView() = delete;

  BoardView(BoardView const &) = delete;
  BoardView & operator= (BoardView const &) = delete;

  BoardView(BoardView &&) noexcept = default;
  BoardView & operator= (BoardView &&) noexcept = default;

  BoardView(Board const & board, SDL_Point position, Config const & config);
  ~BoardView() noexcept = default;

  void DrawBoard(SDL_Renderer * renderer) const noexcept;

  /** Draws a tetromino at an absolute position.
   * The color depends on the tetromino type.
   * @param renderer  SDL renderer used for drawing
   * @param tetromino Tetromino to draw
   * @param x         Absolute 'x' position of the blocks tetromino
   * @param y         Absolute 'y' position of the blocks tetromino
   */
  void DrawTetromino(SDL_Renderer * renderer, Tetromino const & tetromino, int x, int y) const noexcept;

  /** Draws player controlled tetromino at its current position.
   * The color depends on the tetromino type.
   * @param renderer  SDL renderer used for drawing
   * @param tetromino Player tetromino to draw
   */
  void DrawPlayerTetromino(SDL_Renderer * renderer, PlayerTetromino const & tetromino) const noexcept;

  /** Draws the projected ghost tetromino at its current position.
   * @param renderer  SDL renderer used for drawing
   * @param ghost     Ghost tetromino to draw
   */
  void DrawGhostTetromino(SDL_Renderer * renderer, PlayerTetromino const & ghost) const noexcept;

  /** Draws the given line of within the board/grid.
   * @param renderer  SDL renderer used for drawing
   * @param row       The number of the row to be drawn
   */
  void DrawBlockLine(SDL_Renderer * renderer, int row) const noexcept;

  /** Draws tetromino within the board.
   * Uses the current color set within the renderer.
   * @param renderer  SDL renderer used for drawing
   * @param tetromino Tetromino to draw
   */
  void DrawTetrominoWithinBoard(SDL_Renderer * renderer,
                                PlayerTetromino const & tetromino) const noexcept;

private:
  /** Draws tetrominos block.
   * The color depends on the tetromino type.
   * @param renderer  SDL renderer used for drawing
   * @param block     Coordinates of the block within the tetromino
   * @param position  Absolute position of the blocks tetromino
   */
  void DrawBlock(SDL_Renderer * renderer,
                 SDL_Point const & block,
                 SDL_Point const & position) const noexcept;

  constexpr SDL_Point block_position(int x, int y) const noexcept {
    return SDL_Point{
      position_.x + x * (block_size_ + block_gap_),
      position_.y + (y - board_.buffer_height()) * (block_size_ + block_gap_)};
  }
  constexpr SDL_Rect block_rectangle(int x, int y) const noexcept {
    return SDL_Rect{
      block_position(x, y).x,
      block_position(x, y).y,
      block_size_,
      block_size_};
  }

  void DrawDebugRectangleAroundTiles(SDL_Renderer * renderer, int x, int y, int w, int h) const noexcept;

private:
  /// Board position on the screen
  SDL_Point const position_;
  /// The size of the gap between blocks (in pixels)
  int const block_gap_;
  /// The size of each block (in pixels)
  int const block_size_;
  /// If true, tetromino blocks within the top (spawn) buffer are displayed
  bool display_buffer_;

  /// Reference to actual board
  Board const & board_;
}; // class BoardView

} // namespace tajga
