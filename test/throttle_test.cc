#include "catch2/catch.hpp"

#include "throttle.h"

namespace {

int ThrottleTest(float times_per_second, int duration_in_seconds = 10, int ticks_per_second = 25) {
  tajga::Throttle t{times_per_second, ticks_per_second};

  int c = 0;
  for (int i = 0; i < ticks_per_second * duration_in_seconds; ++i) {
    t.Call([&c](){ ++c; });
  }
  return c;
}

} // namespace

TEST_CASE("Throttled calls", "[throttle]") {
  SECTION("0 times per second") {
    REQUIRE(ThrottleTest(0.0f, 10, 25) == 0);
    REQUIRE(ThrottleTest(0.0f,  2, 25) == 0);
    REQUIRE(ThrottleTest(0.0f, 10, 30) == 0);
  }

  SECTION("0.5 times per second") {
    REQUIRE(ThrottleTest(0.5f, 10, 25) == 5);
    REQUIRE(ThrottleTest(0.5f,  2, 25) == 1);
    REQUIRE(ThrottleTest(0.5f, 10, 30) == 5);
  }

  SECTION("1 times per second") {
    REQUIRE(ThrottleTest(1.0f, 10, 25) == 10);
    REQUIRE(ThrottleTest(1.0f,  2, 25) ==  2);
    REQUIRE(ThrottleTest(1.0f, 10, 30) == 10);
  }

  SECTION("1.5 times per second") {
    REQUIRE(ThrottleTest(1.5f, 10, 25) == 15);
    REQUIRE(ThrottleTest(1.5f,  2, 25) ==  3);
    REQUIRE(ThrottleTest(1.5f, 10, 30) == 15);
  }

  SECTION("5 times per second") {
    REQUIRE(ThrottleTest(5.0f, 10, 25) == 50);
    REQUIRE(ThrottleTest(5.0f,  2, 25) == 10);
    REQUIRE(ThrottleTest(5.0f, 10, 30) == 50);
  }

  SECTION("10 times per second") {
    REQUIRE(ThrottleTest(10.0f, 10, 25) == 100);
    REQUIRE(ThrottleTest(10.0f,  2, 25) ==  20);
    REQUIRE(ThrottleTest(10.0f, 10, 30) == 100);
  }

  SECTION("20 times per second") {
    REQUIRE(ThrottleTest(20.0f, 10, 25) == 200);
    REQUIRE(ThrottleTest(20.0f,  2, 25) ==  40);
    REQUIRE(ThrottleTest(20.0f, 10, 30) == 200);
  }
}

TEST_CASE("Reset throttle resets the current progress", "[throttle]") {
  auto const duration_in_seconds = 10;
  auto const ticks_per_second = 25;

  tajga::Throttle t{5.0f, ticks_per_second};

  int c = 0;
  for (int i = 0; i < ticks_per_second * duration_in_seconds; ++i) {
    t.Call([&c](){ ++c; });
    t.Reset();
  }
  REQUIRE(c == 0);
}

