#define CATCH_CONFIG_RUNNER
#include "catch2/catch.hpp"

#include <SDL2/SDL_log.h>

int main( int argc, char* argv[] ) {
  // global setup...
  SDL_LogSetAllPriority(SDL_LOG_PRIORITY_CRITICAL);

  int result = Catch::Session().run( argc, argv );

  // global clean-up...

  return result;
}
