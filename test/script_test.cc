#include "catch2/catch.hpp"

#include "script.h"

TEST_CASE("name parsing", "[script]") {
  tajga::ScriptEngine s;
  s.DoString(R"(
simple_variable = 1
scope = {
  nested_variable = 2,

  inner_scope = {
    super_nested_variable = 3
  }
})");

  SECTION("Simple name") {
    CHECK(s.get<int>("simple_variable") == 1);
  }

  SECTION("Can not access nested variable without its scope") {
    CHECK_THROWS_WITH(s.get<int>("nested_variable"),  "'nested_variable': Invalid LUA variable name");
    CHECK_THROWS_WITH(s.get<int>("super_nested_variable"),   "'super_nested_variable': Invalid LUA variable name");
  }

  SECTION("Can access nested variable with its scope") {
    CHECK(s.get<int>("scope.nested_variable") == 2);
    CHECK(s.get<int>("scope.inner_scope.super_nested_variable") == 3);
  }
}

TEST_CASE("get functionality", "[script]") {
  tajga::ScriptEngine s;
  s.DoString(R"(
config = {
  string_variable = "text",
  int_variable = 42,
  float_variable = 3.14,
  bool_variable_true = true,
  bool_variable_false = false,
})");

  SECTION("invalid name") {
    CHECK_THROWS_WITH(s.get<bool>("invalid"), "'invalid': Invalid LUA variable name");
    CHECK_THROWS_WITH(s.get<int>("invalid2"), "'invalid2': Invalid LUA variable name");
    CHECK_THROWS_WITH(s.get<std::string>("config.invalid"), "'config.invalid': Invalid LUA variable name");
    CHECK_THROWS_WITH(s.get<std::string>("config.string_variable.invalid"),
                      "'config.string_variable.invalid': Invalid LUA variable name");
  }

  SECTION("get bool") {
    CHECK(s.get<bool>("config.bool_variable_true"));
    CHECK_FALSE(s.get<bool>("config.bool_variable_false"));
    CHECK_THROWS_WITH(s.get<bool>("config.string_variable"), "'config.string_variable': Boolean variable expected");
    CHECK_THROWS_WITH(s.get<bool>("config.int_variable"), "'config.int_variable': Boolean variable expected");
    CHECK_THROWS_WITH(s.get<bool>("config.float_variable"), "'config.float_variable': Boolean variable expected");
  }

  SECTION("get char") {
    CHECK(s.get<char>("config.int_variable") == 42);
    CHECK_THROWS_WITH(s.get<char>("config.string_variable"), "'config.string_variable': Integral variable expected");
    CHECK_THROWS_WITH(s.get<char>("config.float_variable"), "'config.float_variable': Integral variable expected");
    CHECK_THROWS_WITH(s.get<char>("config.bool_variable_true"), "'config.bool_variable_true': Integral variable expected");
    CHECK_THROWS_WITH(s.get<char>("config.bool_variable_false"), "'config.bool_variable_false': Integral variable expected");
  }

  SECTION("get int") {
    CHECK(s.get<int>("config.int_variable") == 42);
    CHECK_THROWS_WITH(s.get<int>("config.string_variable"), "'config.string_variable': Integral variable expected");
    CHECK_THROWS_WITH(s.get<int>("config.float_variable"), "'config.float_variable': Integral variable expected");
    CHECK_THROWS_WITH(s.get<int>("config.bool_variable_true"), "'config.bool_variable_true': Integral variable expected");
    CHECK_THROWS_WITH(s.get<int>("config.bool_variable_false"), "'config.bool_variable_false': Integral variable expected");
  }

  SECTION("get int64_t") {
    CHECK(s.get<int64_t>("config.int_variable") == 42);
    CHECK_THROWS_WITH(s.get<int64_t>("config.string_variable"), "'config.string_variable': Integral variable expected");
    CHECK_THROWS_WITH(s.get<int64_t>("config.float_variable"), "'config.float_variable': Integral variable expected");
    CHECK_THROWS_WITH(s.get<int64_t>("config.bool_variable_true"), "'config.bool_variable_true': Integral variable expected");
    CHECK_THROWS_WITH(s.get<int64_t>("config.bool_variable_false"), "'config.bool_variable_false': Integral variable expected");
  }

  SECTION("get float") {
    CHECK(s.get<float>("config.int_variable") == Approx(42));
    CHECK(s.get<float>("config.float_variable") == Approx(3.14));
    CHECK_THROWS_WITH(s.get<float>("config.string_variable"), "'config.string_variable': Floating point variable expected");
    CHECK_THROWS_WITH(s.get<float>("config.bool_variable_true"), "'config.bool_variable_true': Floating point variable expected");
    CHECK_THROWS_WITH(s.get<float>("config.bool_variable_false"), "'config.bool_variable_false': Floating point variable expected");
  }

  SECTION("get double") {
    CHECK(s.get<double>("config.int_variable") == Approx(42));
    CHECK(s.get<double>("config.float_variable") == Approx(3.14));
    CHECK_THROWS_WITH(s.get<double>("config.string_variable"), "'config.string_variable': Floating point variable expected");
    CHECK_THROWS_WITH(s.get<double>("config.bool_variable_true"), "'config.bool_variable_true': Floating point variable expected");
    CHECK_THROWS_WITH(s.get<double>("config.bool_variable_false"), "'config.bool_variable_false': Floating point variable expected");
  }

  SECTION("get string") {
    CHECK(s.get<std::string>("config.string_variable") == "text");
    CHECK(s.get<std::string>("config.int_variable") == "42");
    CHECK(s.get<std::string>("config.float_variable") == "3.14");
    CHECK_THROWS_WITH(s.get<std::string>("config.bool_variable_true"), "'config.bool_variable_true': String variable expected");
    CHECK_THROWS_WITH(s.get<std::string>("config.bool_variable_false"), "'config.bool_variable_false': String variable expected");
  }
}

TEST_CASE("get SDL_Point", "[script]") {
  tajga::ScriptEngine s;
  s.DoString(R"(
i = 42
a0 = {22}
a1 = {22, 44, 66, 88}
p0 = {22, 44}
scope = {
  p1 = {2, 8},
  p2 = {'x', 2},
  p3 = {1, 'x'},
})");

  SECTION("Invalid value type") {
    CHECK_THROWS_WITH(s.get<SDL_Point>("i"), "'i': SDL_Point variable expected (table with two values)");
  }

  SECTION("Invalid number of values") {
    CHECK_THROWS_WITH(s.get<SDL_Point>("a0"), "'a0': SDL_Point variable expected (other than two table values found)");
    CHECK_THROWS_WITH(s.get<SDL_Point>("a1"), "'a1': SDL_Point variable expected (other than two table values found)");
  }

  SECTION("Invalid table value type") {
    CHECK_THROWS_WITH(s.get<SDL_Point>("scope.p2"), "'scope.p2.x': Integer variable expected");
    CHECK_THROWS_WITH(s.get<SDL_Point>("scope.p3"), "'scope.p3.y': Integer variable expected");
  }

  SECTION("Proper SDL_Point retrieval") {
    auto const p0 = s.get<SDL_Point>("p0");
    CHECK(p0.x == 22);
    CHECK(p0.y == 44);

    auto const p1 = s.get<SDL_Point>("scope.p1");
    CHECK(p1.x == 2);
    CHECK(p1.y == 8);
  }
}

