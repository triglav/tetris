#include "catch2/catch.hpp"

#include "block.h"
#include "tetromino.h"

inline bool operator== (SDL_Point const & lhs, SDL_Point const & rhs) {
  return lhs.x == rhs.x && lhs.y == rhs.y;
}
inline bool operator!= (SDL_Point const & lhs, SDL_Point const & rhs) {
  return !operator==(lhs, rhs);
}

namespace tajga {
bool operator== (Tetromino const & lhs, Tetromino const & rhs) {
  return lhs.type() == rhs.type() && lhs.shape() == rhs.shape();
}
bool operator!= (Tetromino const & lhs, Tetromino const & rhs) {
  return !operator==(lhs, rhs);
}
} // namespace tajga

using tajga::Block;
using tajga::Tetromino;

TEST_CASE("Tetromino BuildShape", "[tetromino]") {
  auto const t = Tetromino(Block::I, 0);
  REQUIRE(Block::I == t.type());
  REQUIRE(4 == t.shape().size());

  auto const p1 = SDL_Point{0, 1};
  auto const p2 = SDL_Point{1, 1};
  auto const p3 = SDL_Point{2, 1};
  auto const p4 = SDL_Point{3, 1};

  REQUIRE(p1 == t.shape()[0]);
  REQUIRE(p2 == t.shape()[1]);
  REQUIRE(p3 == t.shape()[2]);
  REQUIRE(p4 == t.shape()[3]);
}

TEST_CASE("Tetromino BuildShape2", "[tetromino]") {
  auto t = Tetromino(Block::I, 1);
  REQUIRE(Block::I == t.type());
  REQUIRE(4 == t.shape().size());

  auto const p1 = SDL_Point{2, 0};
  auto const p2 = SDL_Point{2, 1};
  auto const p3 = SDL_Point{2, 2};
  auto const p4 = SDL_Point{2, 3};

  REQUIRE(p1 == t.shape()[0]);
  REQUIRE(p2 == t.shape()[1]);
  REQUIRE(p3 == t.shape()[2]);
  REQUIRE(p4 == t.shape()[3]);
}

TEST_CASE("Tetromino RotateLeft", "[tetromino]") {
  for (auto t = static_cast<char>(Block::First);
      t < static_cast<char>(Block::Count); ++t) {
    if (static_cast<Block>(t) == Block::O) {
      continue;
    }

    SECTION(std::string("Tetromino ") +
        tajga::to_string(static_cast<Block>(t))) {
      auto const t1 = Tetromino(static_cast<Block>(t), 0);
      auto const t2 = Tetromino(static_cast<Block>(t), 1);
      auto const t3 = Tetromino(static_cast<Block>(t), 2);
      auto const t4 = Tetromino(static_cast<Block>(t), 3);

      REQUIRE(t1 != t2);
      REQUIRE(t2 != t3);
      REQUIRE(t3 != t4);
      REQUIRE(t4 != t1);

      REQUIRE(t1.RotateLeft() == t4);
      REQUIRE(t2.RotateLeft() == t1);
      REQUIRE(t3.RotateLeft() == t2);
      REQUIRE(t4.RotateLeft() == t3);
    }
  }
  SECTION("Tetromino O") {
    auto const t = Tetromino(Block::O, 0);

    REQUIRE(t.RotateLeft() == t);
  }
}

TEST_CASE("Tetromino RotateRight", "[tetromino]") {
  for (auto t = static_cast<char>(Block::First);
      t < static_cast<char>(Block::Count); ++t) {
    if (static_cast<Block>(t) == Block::O) {
      continue;
    }

    SECTION(std::string("Tetromino ") +
        tajga::to_string(static_cast<Block>(t))) {
      auto const t1 = Tetromino(static_cast<Block>(t), 0);
      auto const t2 = Tetromino(static_cast<Block>(t), 1);
      auto const t3 = Tetromino(static_cast<Block>(t), 2);
      auto const t4 = Tetromino(static_cast<Block>(t), 3);

      REQUIRE(t1 != t2);
      REQUIRE(t2 != t3);
      REQUIRE(t3 != t4);
      REQUIRE(t4 != t1);

      REQUIRE(t1.RotateRight() == t2);
      REQUIRE(t2.RotateRight() == t3);
      REQUIRE(t3.RotateRight() == t4);
      REQUIRE(t4.RotateRight() == t1);
    }
  }
  SECTION("Tetromino O") {
    auto const t = Tetromino(Block::O, 0);

    REQUIRE(t.RotateRight() == t);
  }
}
