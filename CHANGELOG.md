# Change Log

## [0.4.0] - 2018-08-12
### Added
- Hold piece
- Score (Clears, T-Spins, Back-to-Back, Combo, Soft/Hard drop)
- Row clear animation
- Lock delay
- Game pause
- Game texts and labels

### Changed
- Hard drop and rotations are no longer chained together while holding a key
- Tetrominos now spawn below the buffer if possible
- 3 next tetrominos

## [0.3.0] - 2018-08-05
### Added
- Ghost piece
- Soft drop and Rotate left actions
- Delayed Auto Shift
- Wall kicks
- LUA config file

### Changed
- Tetromino movement is no longer hardware dependant
- Improved logging and asserts
- Gravity counter resets after soft drop

## [0.2.0] - 2016-11-20
### Added
- Bag of 7 random generator (7 system)
- Levels and difficulty

### Changed
- S,Z or O tetrominos will no longer spawn as the very first piece of the game
- Tetrominos now spawn with their flat side pointed down
- Tetrominos now spawn in a hidden buffer zone

## [0.1.0] - 2016-11-08
### Added
- Initial working version of a Tetris game.
